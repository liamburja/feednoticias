@extends('layout.app')
@yield('showlist')
@section('feednoticias', 'Showlist')

@section('sidebar')
    @parent
<style media="screen">
  table tr{
    border:1px solid black;
  }
  button{
    width: 100px;
    height: 30px;
  }
  .button_insert{
    margin-left: 75%;
  }
  table{
    margin-top: 1%;
  }
</style>
@endsection

@section('content')
      <h2 class="news">Noticias</h2>
      <a href="{{ Route('feed.create') }}">
        <button type="button" name="button" class="button_insert col-lg-3" >Insertar</button>
      </a>
      <table>
        @foreach ($feeds as $feed)
              <tr>
                <td>
                  {{ $feed['title'] }}
                </td>
                <td>
                  <a href="{{ Route('feed.show', $feed->id) }}">
                    <button type="button" name="button">Leer</button>
                  </a>
                </td>
                <td>
                  <a href="{{ Route('feed.edit', $feed->id) }}">
                    <button type="button" name="button">Editar</button>
                  </a>
                </td>
                <td>
                  <a href="{{ Route('feed.destroy', $feed->id) }}">
                    <button type="button" name="button">Eliminar</button>
                  </a>
                </td>
              </tr>
        @endforeach
      </table>

@endsection
