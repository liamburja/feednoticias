@extends('layout.app')
@section('feednoticias', 'Show')
@section('sidebar')
    @parent

@endsection
<style media="screen">
  a{
    text-decoration: none;
  }
</style>
@section('content')
    <h1>Ver noticia</h1>
    <div class="show_new_view">
      <h2>
         {{ $feed['title'] }}
      </h2>
      <p>{{ $feed['description'] }}</p>
      <p>{{ $feed['image'] }}</p>
      <a href="{{ $feed['link'] }}">{{ $feed['link'] }}</a>
      <a href="{{ $feed['publisher'] }}">{{ $feed['publisher'] }}</a>
    </div>
      <div class="show_new_button">
        <a href="{{ route('feed.index')}}"
          <button type="button" name="button">Volver</button>
        </a>
        <a href="{{ Route('feed.edit', $feed->id) }}"
          <button type="button" name="button">Editar</button>
        </a>
        <a href="{{ Route('feed.destroy', $feed->id) }}"
          <button type="button" name="button">Eliminar</button>
        </a>
      </div>
@endsection
