@extends('layout.app')
@section('feednoticias', 'Insert')
@section('sidebar')
    @parent

@endsection

@section('content')
    <h2>Insertar noticia</h2>
    <div>
      <form action="{{ route('feed.store')}}" method="post" class="col-lg-12">
        {{ csrf_field() }}
        <label class="col-lg-12 col-md-12"> Introduzca el titulo:
          <input name="title" required="true" class="col-lg-12 col-md-12">
        </label>
        <label class="col-lg-12 col-md-12"> Introduzca el cuerpo de la noticia:
          <textarea name="body" rows="8" cols="80" placeholder="Escriba la noticia" required="true" class="col-lg-12 col-md-12"></textarea>
        </label>

        <label class="col-lg-5 col-md-5"> Introduzca la url de la imagen:
          <input name="image" required="true">
        </label>
        <label class="col-lg-6 col-md-6"> Introduzca el link de la noticia:
          <input name="source" required="true">
        </label>
        <label> Introduzca la url del periodico:
          <input name="publisher" required="true">
        </label>
        <input class="col-lg-12" type="submit" name="submit" value="Añadir">
      </form>
    </div>
    <div>
      <a href="{{ route('feed.index')}}"
        <button type="button" name="button">Volver</button>
      </a>
    </div>

@endsection
