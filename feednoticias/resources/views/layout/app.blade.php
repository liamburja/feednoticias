<html>
    <head>
        <title> Feednoticias- @yield('title')</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    </head>
    <body>
        @section('sidebar')
            <h1 style="text-align:center">Feed noticias</h1>
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
