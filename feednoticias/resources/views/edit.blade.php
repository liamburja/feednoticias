@extends('layout.app')
@section('feednoticias', 'Edit')

@section('sidebar')
    @parent

@endsection

@section('content')
    <h2>Editar noticia</h2>
    <div>
      <form action="{{ route('feed.update', $feed->id)}}" method="post" class="col-lg-12">
        {{ csrf_field() }}
        <label class="col-lg-12 col-md-12"> Introduzca el titulo:
          <input class="col-lg-12" name="title" required="true" value="{{ $feed['title'] }}">
        </label>
        <label class="col-lg-12 col-md-12"> Introduzca el cuerpo de la noticia:
          <textarea class="col-lg-12" name="body" rows="8" cols="80" value="{{ $feed['description'] }}" required="true"></textarea>
        </label>
        <label class="col-lg-5 col-md-5"> Introduzca la url de la imagen:
          <input name="image" required="true" value="{{ $feed['image'] }}">
        </label>
        <label class="col-lg-6 col-md-6"> Introduzca el link de la noticia:
          <input name="source" required="true" value="{{ $feed['link'] }}">
        </label>
        <label> Introduzca la url del periodico:
          <input name="publisher" required="true" value="{{ $feed['publisher'] }}">
        </label>
        <input class="col-lg-12 button" type="submit" name="submit" value="Añadir">
      </form>
    </div>
    <div>
      <a href="{{ route('feed.index')}}"
        <button type="button" name="button">Volver</button>
      </a>
    </div>
@endsection
