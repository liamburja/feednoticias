<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
  //Añadimos los atributos que va a contener nuestro modelo
    protected $fillable = ['title', 'body', 'image', 'source', 'publisher', 'created_at', 'updated_at'];
}
