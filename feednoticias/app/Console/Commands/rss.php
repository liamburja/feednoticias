<?php

namespace App\Console\Commands;
use XmlParser;
use Illuminate\Console\Command;
use App\Feed;
//Creacion del comando para llamar al rss y volcarlo todo a la base de datos.
class Rss extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:rss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $xml = XmlParser::load('http://estaticos.elmundo.es/elmundo/rss/espana.xml');

        $feeds = $xml->parse([
          'items' => ['uses' => 'channel.item[title,::description,image,link]'],
          'publisher' => ['uses' => 'channel.link'],
          //'followers' => ['uses' => 'user::followers'],
          ]);
          //dd($feeds);

          foreach ($feeds['items'] as $feed) {
            Feed::insert(array('title' =>$feed['title'], 'body' =>$feed['title'], 'image' =>$feed['title'], 'source' =>$feed['link'], 'publisher' =>$feeds['publisher']));
          }
          return view('welcome');
    }
}
