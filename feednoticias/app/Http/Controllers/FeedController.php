<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feed;
use Illuminate\Database\Query\Builder;
//Se añaden todos los controladores editar, eliminar, insertar... para las noticias
class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $feeds=Feed::all();
      //dd($feeds);
      return view('showlist',['feeds' => $feeds]);
      //array(‘feed’ => $feeds)
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[ 'title'=>'required', 'body'=>'required', 'image'=>'required', 'source'=>'required', 'publisher'=>'required']);
      Feed::create($request->all());
      return redirect()->route('index')->with('success','Registro creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $feed=Feed::find($id);
      return  view('show',compact('feed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $feed=Feed::find($id);
      return view('edit',compact('feed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[ 'title'=>'required', 'body'=>'required', 'image'=>'required', 'source'=>'required', 'publisher'=>'required']);

      Feed::find($id)->update($request->all());
      return redirect()->route('index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Feed::find($id)->delete();
      return redirect()->route('index')->with('success','Registro eliminado satisfactoriamente');
    }
}
