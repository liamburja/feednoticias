<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    Artisan::call('import:rss');
    return view('index');
});
//Añadimos la ruta del modelo con el controlador
Route::resource('feed', 'feedController');

Route::get('rss', function () {
    Artisan::call('import:rss');
});
//Route::get('/showlist', function () {
//    return view('showlist');
//});
//Route::get('/insert', function () {
//    return view('insert');
//});
//Route::
Route::get('store', 'feedController@store');
//Route::resource('showlist', 'feedController');
